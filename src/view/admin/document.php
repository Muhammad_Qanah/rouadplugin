<div class="col-md-12">
    <h2><?= __('functions documention', 'sso') ?></h2>
    <div class="panel panel-default">
        <div class="panel-heading font_bold panel-height">
        <div class="col-md-3 font-bold"><?= __('filter name', 'sso') ?></div>
        <div class="col-md-3 font-bold"><?= __('arguments', 'sso') ?></div>
        <div class="col-md-3 font-bold"><?= __('return', 'sso') ?></div>
        <div class="col-md-3 font-bold"><?= __('description', 'sso') ?></div>
        </div>
        <div class="panel-body panel-scrol">
            <div class="col-md-3">set_user_session</div>
            <div class="col-md-3">[id,email,token]</div>
            <div class="col-md-3">user id or -1</div>
            <div class="col-md-3">set session for login or singup user sso</div>
            <div class="between"></div>
            <div class="col-md-3">check_user_connect</div>
            <div class="col-md-3">NULL</div>
            <div class="col-md-3">TRUE OR FALSE</div>
            <div class="col-md-3">check if user connect to sso system</div>
            <div class="between"></div>
            <div class="col-md-3">check_user_updated</div>
            <div class="col-md-3">NULL</div>
            <div class="col-md-3">array of data for user updated</div>
            <div class="col-md-3">check if user update in sso system</div>
            <div class="between"></div>
            <div class="col-md-3">confirm_user_updated</div>
            <div class="col-md-3">NULL</div>
            <div class="col-md-3">empty</div>
            <div class="col-md-3">confirm user updated in website and send to sso</div>
            <div class="between"></div>
            <div class="col-md-3">add_offer</div>
            <div class="col-md-3">[id,prices[],sale_prices[],start_date
                ,end_date]</div>
            <div class="col-md-3">array detalis product</div>
            <div class="col-md-3">add offer to sso</div>
            <div class="between"></div>
            <div class="col-md-3">get_product_price</div>
            <div class="col-md-3">[id,currency]</div>
            <div class="col-md-3">price,sale_price</div>
            <div class="col-md-3">get product price by id</div>
            <div class="between"></div>
            <div class="col-md-3">get_members_info</div>
            <div class="col-md-3">page</div>
            <div class="col-md-3">array from subscribers info</div>
            <div class="col-md-3">get all subscribers info</div>
            <div class="between"></div>
            <div class="col-md-3">get_members_by_id</div>
            <div class="col-md-3">id</div>
            <div class="col-md-3">array from subscribers info</div>
            <div class="col-md-3">get all subscriber info</div>
            <div class="between"></div>
            <div class="col-md-3">check_post_to_show</div>
            <div class="col-md-3">[id,post_type]</div>
            <div class="col-md-3">array contains valid and remaining posts to view</div>
            <div class="col-md-3">verify that a specific post has been read </div>
            <div class="between"></div>
            <div class="col-md-3">get_all_content</div>
            <div class="col-md-3">NULL</div>
            <div class="col-md-3">array contains all content</div>
            <div class="col-md-3">get all conent (article,voice,video..</div>
            <div class="between"></div>
            <div class="col-md-3">get_content_by_id</div>
            <div class="col-md-3">id</div>
            <div class="col-md-3">array contains content</div>
            <div class="col-md-3">get content (article,voice,video.. by id</div>
            <div class="between"></div>
            <div class="col-md-3">add_bulk_content</div>
            <div class="col-md-3">collection of array[content_id,content_type
                ,content_is_paid,content_cost]</div>
            <div class="col-md-3">array contains all content</div>
            <div class="col-md-3">add collection of content</div>
            <div class="between"></div>
            <div class="col-md-3">add_content</div>
            <div class="col-md-3">array[content_id,content_type
                ,content_is_paid,content_cost]</div>
            <div class="col-md-3">array content</div>
            <div class="col-md-3">add new content</div>
            <div class="between"></div>
            <div class="col-md-3">update_content</div>
            <div class="col-md-3">array[content_id,content_type
                ,content_is_paid,content_cost]</div>
            <div class="col-md-3">array content</div>
            <div class="col-md-3">edit content</div>
            <div class="between"></div>
            <div class="col-md-3">delete_content</div>
            <div class="col-md-3">content_id</div>
            <div class="col-md-3">array content</div>
            <div class="col-md-3">delete content</div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <h2><?= __('widget documention', 'sso') ?></h2>
    <div class="panel panel-default">
        <div class="panel-heading font_bold panel-height">
            <div class="col-md-3 font-bold"><?= __('filter name', 'sso') ?></div>
            <div class="col-md-3 font-bold"><?= __('arguments', 'sso') ?></div>
            <div class="col-md-3 font-bold"><?= __('description', 'sso') ?></div>
        </div>
        <div class="panel-body">
            <div class="col-md-3">signin-view</div>
            <div class="col-md-3">-</div>
            <div class="col-md-6">signin or signup to sso</div>
            <div class="between"></div>
            <div class="col-md-3">profile-views</div>
            <div class="col-md-3">-</div>
            <div class="col-md-6">access to profile,subscriber,address,adddress and logout user</div>
            <div class="between"></div>
            <div class="col-md-3">article-lock-views</div>
            <div class="col-md-3">-</div>
            <div class="col-md-6">signin or signup to sso</div>
        </div>
    </div>
</div>