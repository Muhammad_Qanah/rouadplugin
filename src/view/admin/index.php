<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading font-weight-bold main_title"><?= __('About sso plugin', 'sso') ?></div>
    </div>
    <div class="panel-footer">
        <p class="got_to_setting"> <?= __('go to', 'sso') ?> <a href="<?= admin_url("admin.php?page=setting")  ?>"><?= __('Setting', 'sso') ?></a> </p>
    </div>
</div>