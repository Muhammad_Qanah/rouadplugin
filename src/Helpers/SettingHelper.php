<?php

namespace Sso\WP\Helpers;

class SettingHelper
{
    private static $instance;
    private $isAuth;
    private $PurchKey;
    private $error;

    private function __construct()
    {
        $this->api = FunctionApiHelper::getInstance();
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new SettingHelper();
        }
        return self::$instance;
    }

    public function getPurchKey()
    {
        return $this->PurchKey;
    }

    public function setPurchKey($PurchKey)
    {
        $this->PurchKey = $PurchKey;
    }


    public function getError()
    {
        return $this->error;
    }

    public function setError($error)
    {
        $this->error = $error;
    }
 public function checkAuth(){
     // check purchKey
     $wp_option = get_option('purchase_key');
    /// whcom_whmcs_admin_url
     if (!($wp_option)) return false;
     return true;
 }
    public function generatePurchKey()
    {
        global $wpdb;

        if ($wpdb->prefix == 'hbrar_') {
            $perches_key = "RH1IBbYgRwO44VFPClzpPnCR2Rx3b7gcOqymxdaD";
        } elseif ($wpdb->prefix == 'popscai') {
            $perches_key = "undefiend";
        }
        $this->setPurchKey($perches_key);
    }

    public function checkPurchKey()
    {
        // check purchKey
        $wp_option = get_option('purchase_key');
        if (!($wp_option)) return false;
        return true;
    }

    public function handleSubmit()
    {
//        if(isset($_POST['action']  )){
//         $array= ['action'=>$_POST['action'],'username'=>$_POST['whcom_whmcs_admin_user'],'password'=>$_POST['whcom_whmcs_admin_pass'],'accesskey'=>$_POST['whcom_whmcs_admin_api_key']];
//        //   var_dump($array);
//            $this->checkValidPurchesKey($array);
//
//        }
//        if (isset($_POST['sendPurchKey']) && $_POST['action'] = WHI_AUTHENTICATE) {
//            // check not empty purch_key
//            if ($_POST['purch_key']) {
//                $purch_key = trim($_POST['purch_key']);
//                if ($this->checkValidPurchesKey($purch_key)) {
//                    $this->savePurchKey();
//                } else {
//                    $msg = "purchKey invalid";
//                    $this->setError($msg);
//                    return false;
//                }
//            } else {
//                $msg = "purchKey required";
//                $this->setError($msg);
//                return false;
//            }
//        }
    }

    private function checkValidPurchesKey($array)
    {
        $config = array();
        $result = $this->api->checkPurchKeySso($array);
       // var_dump($result);
        die();

        return ($result->status == "success" && $result->message == "Athenticated") ? true : false;
    }

    public function savePurchKey()
    {
        global $wpdb;
        $var = trim($_POST['purch_key']);
        $table = "hbrar_options";
        $optionId = $this->getLastOption();
        $array = array("option_id" => $optionId, "option_name" => "purchase_key", "option_value" => $var);
        $a = $wpdb->insert($table, $array);
        $url = admin_url('admin.php?page=sso');
        wp_redirect($url);
        exit();
    }


    public function getLastOption()
    {
        global $wpdb;
        $sql = "select option_id from {$wpdb->prefix}options ORDER BY option_id DESC limit 1 ";
        $data = $wpdb->get_results($sql);
        return $data[0]->option_id + 1;
    }

    public function getNameWidgets()
    {
        return ["Signin", "Profile-subscriber", "ArticleLock"];
    }


    private function checkFoundWidgets($name)
    {
        return (get_option($name)) ? true : false;
    }


    public function setWidgets($name)
    {
        if (!$this->checkFoundWidgets($name)) {
            global $wpdb;
            $name = 'Widget-' . $name;
            $table = "{$wpdb->prefix}options";
            $optionId = $this->getLastOption();
            $style = $this->widgetsStyle($name);
            $array = array("option_id" => $optionId, "option_name" => $name, "option_value" => $style);
            $wpdb->insert($table, $array);
        }
    }

    private function widgetsStyle($name)
    {
        if ($name == "Widget-Signin") {
            $style = array("style" => "", "background" => "#f8f0ff", "fontsize" => "16px", "fontcolor" => "#5a00a0");
        } elseif ($name == "Widget-ArticleLock") {
            $style = array("style" => "", "background" => "#fff", "fontsize" => "1rem", "fontcolor" => "#212529");
        } elseif ($name = "Widget-Profile-subscriber") {
            $style = array("style" => "", "background" => "#5a00a0", "fontsize" => "16px", "fontcolor" => "black");
        } else {
            $style = array("style" => "", "background" => "white", "fontsize" => "15px", "fontcolor" => "black");
        }
        $result = wp_json_encode($style);

        return $result;
    }

    public function getWidgets()
    {
        global $wpdb;
        $data = $wpdb->get_results("SELECT option_id,option_name from  {$wpdb->prefix}options WHERE option_name  LIKE 'Widget-%'");
        return $data;
    }

}